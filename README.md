# DiscUnit.js

[![Built With Love](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![Code Style: Standard](https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=for-the-badge)](https://standardjs.com)
[![Discord.JS](https://img.shields.io/badge/discord-js-blue.svg?style=for-the-badge)](https://discord.js.org/#/)
[![Gitter](https://img.shields.io/badge/chat-on_gitter-1dce73.svg?logo=gitter-white&style=for-the-badge)](https://gitter.im/DiscUnit-js/Lobby)

An API for unit testing with JavaScript Discord bots.

```
yarn add --dev discunit.js
```

## Example

```js
const test = require('discunit.js')
const config = require('./secretConfig')
// Initialize your bot...
test(
  t => {
    t.messageContentEqualTo('pong')
    this.client.channels.get(this.channel).send('ping')
    // Awaits a response of "pong"
  },
  'Ping test',
  ...config
)
```
