/** Basic unit testing functions */
class UnitTest {
  /**
   * Initializes a unit test
   * @constructor
   * @param {number} count - The amount of tests to be run
   */
  constructor (count) {
    this.count = 0
    this.end = count
  }
  /**
   * Assert a value to be true
   * @param {boolean} value - The value to test
   * @param {string} [comment] - A description of the test
   */
  assert (value, comment) {
    this.count++
    if (value) {
      console.log(
        `#${this.count} Assertion (Passed)${comment ? ` - ${comment}` : ''}`
      )
    } else {
      console.log(
        `#${this.count}  Assertion (Failed)${comment ? ` - ${comment}` : ''}`
      )
      console.log(`Expected true but got ${Boolean(value)}`)
      throw new Error(`#${this.count} failed`)
    }
    if (this.count === this.end) process.exit()
    return this
  }
  /**
   * Assert a value to be false
   * @param {boolean} value - The value to test
   * @param {string} [comment] - A description of the test
   */
  assertFalse (value, comment) {
    this.count++
    if (!value) {
      console.log(
        `#${this.count} False Assertion (Passed)${
          comment ? ` - ${comment}` : ''
        }`
      )
    } else {
      console.log(
        `#${this.count} False Assertion (Failed)${
          comment ? ` - ${comment}` : ''
        }`
      )
      console.log(`Expected false but got ${Boolean(value)}`)
      throw new Error(`#${this.count} failed`)
    }
    if (this.count === this.end) process.exit()
    return this
  }
  /**
   * Test for equality
   * @param {*} value1 - The value that `value2` needs to be equal to
   * @param {*} value2 - The value to test for equality
   * @param {string} [comment] - A description of the test
   */
  equal (value1, value2, comment) {
    this.count++
    if (value1 === value2) {
      console.log(
        `#${this.count} Equality (Passed)${comment ? ` - ${comment}` : ''}`
      )
    } else {
      console.log(
        `#${this.count} Equality (Failed)${comment ? ` - ${comment}` : ''}`
      )
      console.log(`Expected ${value1} but got ${value2}`)
      throw new Error(`#${this.count} failed`)
    }
    if (this.count === this.end) process.exit()
    return this
  }
  /**
   * Test for inequality
   * @param {*} value1 - The value that `value2` needs to not be equal to
   * @param {*} value2 - The value to test for inequality
   * @param {string} [comment] - A description of the test
   */
  notEqual (value1, value2, comment) {
    this.count++
    if (value1 !== value2) {
      console.log(
        `#${this.count} Inequality (Passed)${comment ? ` - ${comment}` : ''}`
      )
    } else {
      console.log(
        `#${this.count} Inequality (Failed)${comment ? ` - ${comment}` : ''}`
      )
      console.log(`Expected anything but ${value1} but got ${value2}`)
      throw new Error(`#${this.count} failed`)
    }
    if (this.count === this.end) process.exit()
    return this
  }
}
module.exports = UnitTest
